Source: dolphin-plugins
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 5.240.0~),
               gettext,
               libdolphinvcs-dev (>= 4:24.05.2~),
               libkf6config-dev (>= 5.240.0~),
               libkf6coreaddons-dev (>= 5.240.0~),
               libkf6i18n-dev (>= 5.240.0~),
               libkf6kio-dev (>= 5.240.0~),
               libkf6solid-dev (>= 5.240.0~),
               libkf6texteditor-dev (>= 5.240.0~),
               libkf6textwidgets-dev (>= 5.240.0~),
               libkf6xmlgui-dev (>= 5.240.0~),
               qt6-base-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://apps.kde.org/en/dolphin_plugins
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/dolphin-plugins
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/dolphin-plugins.git

Package: dolphin-plugins
Architecture: any
Section: devel
Depends: dolphin (>= 24.05.2~), ${misc:Depends}, ${shlibs:Depends},
Enhances: dolphin,
Description: plugins for Dolphin
 This package contains plugins for Dolphin that enhance its functionalities.
 .
 There are plugins that offer integration with the following version control
 systems:
 .
  * Bzr
  * Git
  * Mercurial
  * Subversion
 .
 Also, there are plugins to offer integration with the following services:
 .
  * Dropbox
 .
 Miscellaneous plugins:
 .
  * Mount ISO
 .
 This package is part of the KDE Software Development Kit module.
